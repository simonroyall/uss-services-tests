﻿using Newtonsoft.Json.Converters;

namespace USS_API_Tests.Serialization
{
    public class DateOnlyConverter : IsoDateTimeConverter
    {
        public DateOnlyConverter()
        {
            DateTimeFormat = "yyyy-MM-dd";
        }
    }
}