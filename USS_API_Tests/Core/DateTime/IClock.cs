﻿namespace USS_API_Tests.Core.DateTime
{
    public interface IClock
    {
        System.DateTime Now();
    }
}