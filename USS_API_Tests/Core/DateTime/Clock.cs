﻿namespace USS_API_Tests.Core.DateTime
{
    public class Clock : IClock
    {
        public System.DateTime Now()
        {
            return System.DateTime.Now;
        }
    }
}