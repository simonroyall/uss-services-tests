﻿namespace USS_API_Tests.Core.Extensions
{
    public static class DoubleExtensions
    {
        public static double PercentageToDecimalValue (this double source)
        {
            return source / 100;
        }
    }
}