﻿using System.IO;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace USS_API_Tests.Core.Helpers
{
    public static class SerializationHelper
    {
        public static async Task<T> DeserializeInputStreamAsync<T>(Stream stream)
        {
            var streamContents = await new StreamReader(stream).ReadToEndAsync();

            return JsonConvert.DeserializeObject<T>(streamContents);
        }
    }
}