﻿namespace USS_API_Tests.Core.Helpers
{
    public static class DateTimeHelper
    {
        public static int CalculateAgeFor(System.DateTime dateOfBirth)
        {
            return CalculateAgeAtDateYearsPortion(dateOfBirth, System.DateTime.Now);
        }

        public static int CalculateAgeAtDateYearsPortion(System.DateTime dateOfBirth, System.DateTime asAtDate)
        {
            var age = asAtDate.Year - dateOfBirth.Year;

            var needsLeapYearAdjustment = dateOfBirth.Date > asAtDate.AddYears(-age);

            if (needsLeapYearAdjustment)
            {
                age--;
            }

            return age;
        }

        public static int CalculateAgeAtDateMonthsPortion(System.DateTime dateOfBirth, System.DateTime asAtDate)
        {
            var ageInYears = CalculateAgeAtDateYearsPortion(dateOfBirth, asAtDate);

            dateOfBirth = dateOfBirth.AddYears(ageInYears);

            var ageInMonths = asAtDate.Month - dateOfBirth.Month;

            if (dateOfBirth.Day > asAtDate.Day)
            {
                ageInMonths--;
            }

            if (ageInMonths < 0)
            {
                ageInMonths = 12 + ageInMonths;
            }

            return ageInMonths;
        }
    }
}