﻿using System;
using JetBrains.Annotations;

namespace USS_API_Tests.Core.Helpers
{
    public static class Assert
    {
        public sealed class ValidatedNotNullAttribute : Attribute { }

        [AssertionMethod]
        public static void NotNull([ValidatedNotNull][AssertionCondition(AssertionConditionType.IS_NOT_NULL)] object target, string message)
        {
            if (target == null)
            {
                throw new InvalidOperationException(message);
            }
        }

        [AssertionMethod]
        public static void NotNullOrZero([AssertionCondition(AssertionConditionType.IS_NOT_NULL)] decimal? target, string field)
        {
            NotNull(target, $"{field} is null");

            if (target.Value <= 0)
            {
                throw new InvalidOperationException($"{field} less than or equal to zero");
            }
        }

        [AssertionMethod]
        public static void NotNullOrZero([AssertionCondition(AssertionConditionType.IS_NOT_NULL)] double? target, string field)
        {
            NotNull(target, $"{field} is null");

            if (target.Value <= 0)
            {
                throw new InvalidOperationException($"{field} less than or equal to zero");
            }
        }

        [AssertionMethod]
        public static void NotNullOrZero([AssertionCondition(AssertionConditionType.IS_NOT_NULL)] int? target, string field)
        {
            NotNull(target, $"{field} is null");

            if (target.Value <= 0)
            {
                throw new InvalidOperationException($"{field} less than or equal to zero");
            }
        }
    }
}