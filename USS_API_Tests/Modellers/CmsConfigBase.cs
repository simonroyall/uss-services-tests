﻿using Newtonsoft.Json;

namespace USS_API_Tests.Modellers
{
    public class CmsConfigBase
    {
        [JsonProperty("cmsVersion")]
        public int CmsVersion { get; set; }
    }
}