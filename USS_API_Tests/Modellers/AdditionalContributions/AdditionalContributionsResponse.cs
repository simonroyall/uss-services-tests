﻿using System.Collections.Generic;

namespace USS_API_Tests.Modellers.AdditionalContributions
{
	public class AdditionalContributionsResponse
	{
		public AdditionalContributionsResponseData Data { get; set; }

       public IEnumerable<string> Errors { get; set; }
	}
}
