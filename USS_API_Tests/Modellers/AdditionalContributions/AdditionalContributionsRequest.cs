﻿using System;
using Newtonsoft.Json;
using USS_API_Tests.Serialization;

namespace USS_API_Tests.Modellers.AdditionalContributions
{
	public class AdditionalContributionsRequest
	{
		[JsonConverter(typeof(DateOnlyConverter))]
		public DateTime DateOfBirth { get; set; }

		public string Gender { get; set; }

		public decimal AnnualSalary { get; set; }

		public bool HasVoluntarySalaryCap { get; set; }

		public decimal VoluntarySalaryCapAmount { get; set; }

		public double AdditionalContribution { get; set; }

		public decimal LumpSumContribution { get; set; }

		public int RetirementAge { get; set; }

		public double InvestmentReturn { get; set; }

		public double InflationIncrease { get; set; }

		public double SalaryIncrease { get; set; }
	}
}
