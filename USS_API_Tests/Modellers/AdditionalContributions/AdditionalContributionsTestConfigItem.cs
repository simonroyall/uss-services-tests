﻿using System;

namespace USS_API_Tests.Modellers.AdditionalContributions
{
	public class AdditionalContributionsTestConfigItem
	{
		public DateTime OriginalStartDateUsed { get; set; }
        
        public DateTime StartDateAsOfToday { get; set; }

        public int StartDateOffsetRequired { get; set; }

        public DateTime DOBOriginal { get; set; }

        public DateTime DOBAdjusted { get; set; }
        
        public string Sex { get; set; }
       
        public decimal Salary { get; set; }
        
        public decimal VSC { get; set; }
        
        public double AVCPercent { get; set; }
        
        public decimal LumpSumAVC { get; set; }
        
        public double InvestmentReturn { get; set; }
        
        public double SalaryIncrease { get; set; }
        
        public double Inflation { get; set; }
        
        public int RetAge { get; set; }
        
        public decimal MonthlyAvc { get; set; }
        
        public decimal ExpectedAnnualAdditionalContributions { get; set; }
        
        public decimal ExpectedLumpSum { get; set; }
        
        public decimal ExpectedTotalAdditionalAmount { get; set; }
        
        public decimal ExpectedTaxRelief { get; set; }
        
        public decimal ExpectedCostToMember { get; set; }
        
        public decimal ExpectedRetirementPot { get; set; }
        
        public decimal ExpectedYearlyRetirementIncome { get; set; }
	}

}
