﻿namespace USS_API_Tests.Modellers.AdditionalContributions
{
	public class AdditionalContributionsResponseData
	{
		public decimal TodayFundValue { get; set; }

        public decimal AnnualPensionAmount { get; set; }

        public decimal LumpSumContributions { get; set; }

        public decimal TaxSavings { get; set; }

		public decimal AdditionalContributions { get; set; }

		public decimal TotalCombinedContributions { get; set; }

		public decimal CostToMember { get; set; }




	}
}
