﻿using Newtonsoft.Json;

namespace USS_API_Tests.Modellers.BenefitsConversion
{
    public class BenefitsConversionCmsConfigData
    {
        [JsonProperty("pruComFactorKey")]
        public string PruComFactorKey { get; set; }

        [JsonProperty("lifetimeAllowance")]
        public decimal LifetimeAllowance { get; set; }
    }
}