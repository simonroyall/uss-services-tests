﻿namespace USS_API_Tests.Modellers.BenefitsConversion
{
    public class BenefitsConversionAdvancedOptions
    {
        public decimal InvestmentBuilderNonPruAmount { get; set; }
        
        public decimal InvestmentBuilderPruOnlyAmount { get; set; }
        
        public bool LeavePruInvested { get; set; }
    }
}