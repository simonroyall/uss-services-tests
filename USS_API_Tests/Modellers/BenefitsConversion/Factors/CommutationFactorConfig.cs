﻿using System.Collections.Generic;

namespace USS_API_Tests.Modellers.BenefitsConversion.Factors
{
    public class CommutationFactorConfig
    {
        public IList<FactorGroup> FactorGroups { get; set; }
    }
}