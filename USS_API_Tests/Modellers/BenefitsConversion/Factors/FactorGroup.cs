﻿using System;
using System.Collections.Generic;

namespace USS_API_Tests.Modellers.BenefitsConversion.Factors
{
    public class FactorGroup
    {
        public DateTime EffectiveDate { get; set; }
        
        public string GroupKey { get; set; }

        public IEnumerable<FactorItem> Factors { get; set; }
    }
}