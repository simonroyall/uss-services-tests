﻿namespace USS_API_Tests.Modellers.BenefitsConversion.Factors
{
    public class FactorItem
    {
        public int Age { get; set; }

        public decimal Factor { get; set; }
    }
}