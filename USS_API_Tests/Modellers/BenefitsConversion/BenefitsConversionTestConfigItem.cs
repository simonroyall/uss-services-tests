﻿using System;
using Newtonsoft.Json;
using USS_API_Tests.Serialization;

namespace USS_API_Tests.Modellers.BenefitsConversion
{
	public class BenefitsConversionTestConfigItem
	{
        [JsonConverter(typeof(DateOnlyConverter))]
        public DateTime DateOfBirth { get; set; }

        public DateTime DateOfRetirement { get; set; }

        public decimal PreCommutationPensionAmount { get; set; }

        public decimal InvestmentBuilderNonPruAmount { get; set; }

        public decimal InvestmentBuilderPruOnlyAmount { get; set; }

        public decimal PruMpavcFundAmount { get; set; }

        public decimal FutureLumpSumPaymentAmount { get; set; }

        public decimal ExpectedMinimumCash { get; set; }
        public decimal ExpectedMaximumCash { get; set; }
        public decimal ExpectedMinumumPension { get; set; }
        public decimal ExpectedMaximumPension { get; set; }
        public decimal ExpectedMaxUncrystallised { get; set; }





    }

}
