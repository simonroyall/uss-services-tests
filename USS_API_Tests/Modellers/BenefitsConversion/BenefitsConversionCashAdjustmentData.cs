﻿using Newtonsoft.Json;

namespace USS_API_Tests.Modellers.BenefitsConversion
{
    public class BenefitsConversionCashAdjustmentData
    {
        [JsonProperty("pruFundAmount")]
        public decimal PruFundTotalAmount { get; set; }
        
        [JsonProperty("invBuildTotalAmount")]
        public decimal InvestmentBuilderTotalAmount { get; set; }
        
        [JsonProperty("comFactor")]
        public decimal ComFactor { get; set; }
        
        [JsonProperty("revComFactor")]
        public decimal ReverseComFactor { get; set; }
        
        [JsonProperty("pruComFactor")]
        public decimal PruComFactor { get; set; }
    }
}