﻿namespace USS_API_Tests.Modellers.BenefitsConversion
{
    /// <summary>
    /// Enum to represent the different 'types' of member used in the Benefits Conversion calculation.
    /// </summary>
    public enum BenefitsConversionMemberType
    {
        /// <summary>
        /// Refers to a member that can take their DC fund, PRU fund, Lump Sum and Pension as cash 
        /// </summary>
        Type1,
        
        /// <summary>
        /// Refers to a member that can take their DC fund, PRU fund and Lump Sum only as cash 
        /// </summary>
        Type2,
        
        /// <summary>
        /// Refers to a member that can take their DC fund and PRU fund only as cash 
        /// </summary>
        Type3,
        
        /// <summary>
        /// Refers to a member that can take their DC fund only as cash 
        /// </summary>
        /// 
        Type4
    }
}