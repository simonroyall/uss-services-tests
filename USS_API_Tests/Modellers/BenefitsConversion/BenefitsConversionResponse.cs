﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace USS_API_Tests.Modellers.BenefitsConversion
{
    public class BenefitsConversionResponse
    {
        public BenefitsConversionResponse()
        {
            CashAdjustmentData = new BenefitsConversionCashAdjustmentData();
        }

        [JsonProperty("maxPensionAmount")]
        public decimal MaxPensionAmount { get; set; }

        [JsonProperty("minPensionAmount")]
        public decimal MinPensionAmount { get; set; }
        
        [JsonProperty("maxCashAmount")]
        public decimal MaxCashAmount { get; set; }
        
        [JsonProperty("uncrystalisedAmount")]
        public decimal UncrystalisedAmount { get; set; }
        
        [JsonProperty("maxUncrystalisedAmount")]
        public decimal MaxUncrystalisedAmount { get; set; }

        [JsonProperty("maxCashLimitExceeded")]
        public bool MaxCashLimitExceeded { get; set; }

        [JsonProperty("lifetimeAllowanceExceeded")]
        public bool LifetimeAllowanceExceeded { get; set; }

        [JsonProperty("cashAdjustmentData")]
        public BenefitsConversionCashAdjustmentData CashAdjustmentData { get; set; }

        public IEnumerable<string> Errors { get; set; }
    }
}