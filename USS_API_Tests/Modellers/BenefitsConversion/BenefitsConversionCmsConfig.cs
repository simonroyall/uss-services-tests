﻿using Newtonsoft.Json;

namespace USS_API_Tests.Modellers.BenefitsConversion
{
    public class BenefitsConversionCmsConfig : CmsConfigBase
    {
        [JsonProperty("configurationData")]
        public BenefitsConversionCmsConfigData Configuration { get; set; }
    }
}