﻿using System;
using USS_API_Tests.Core.Helpers;

namespace USS_API_Tests.Modellers.BenefitsConversion
{
    public class BenefitsConversionRequest
    {
        public DateTime DateOfBirth { get; set; }

        public DateTime DateOfRetirement { get; set; }

        public decimal PreCommutationPensionAmount { get; set; }

        public decimal InvestmentBuilderNonPruAmount { get; set; }

        public decimal InvestmentBuilderPruOnlyAmount { get; set; }

        public decimal PruMpavcFundAmount { get; set; }

        public decimal FutureLumpSumPaymentAmount { get; set; }

        public BenefitsConversionAdvancedOptions AdvancedOptions { get; set; }

        public bool IsAdvancedModellingRequest => AdvancedOptions != null;

        public decimal TotalInvestmentBuilderPruAmount =>
            IsAdvancedModellingRequest
                ? InvestmentBuilderPruOnlyAmount - AdvancedOptions.InvestmentBuilderPruOnlyAmount
                : InvestmentBuilderPruOnlyAmount;

        public decimal TotalInvestmentBuilderNonPruAmount =>
            IsAdvancedModellingRequest
                ? InvestmentBuilderNonPruAmount + FutureLumpSumPaymentAmount - AdvancedOptions.InvestmentBuilderNonPruAmount
                : InvestmentBuilderNonPruAmount + FutureLumpSumPaymentAmount;

        public decimal TotalPruMpavcFundAmount =>
            IsAdvancedModellingRequest && AdvancedOptions.LeavePruInvested
                ? 0
                : PruMpavcFundAmount;

        public int RetirementAgeInYears => DateTimeHelper.CalculateAgeAtDateYearsPortion(DateOfBirth,DateOfRetirement);
    }
}