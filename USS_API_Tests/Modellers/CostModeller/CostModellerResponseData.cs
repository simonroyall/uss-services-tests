﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace USS_API_Tests.Modellers.CostModeller
{
	[JsonObject]
	public class CostModellerResponseData
	{
		[JsonProperty("memMthSchemeConts")]
		public int MemberMonthlySchemeContributions { get; set; }
		[JsonProperty("memAnnSchemeConts")]
		public int MemberAnnualSchemeContributions { get; set; }
		[JsonProperty("memMthAddInvBuildConts")]
		public int MonthlyAdditionalInvestmentBuilderContributions { get; set; }
		[JsonProperty("memAnnAddInvBuildConts")]
		public int AnnualAdditionalInvestmentBuilderContributions { get; set; }
		[JsonProperty("memAnnNormalInvBuildConts")]
		public int AnnualNormalInvestmentBuilderContributions { get; set; }
		[JsonProperty("empAnnNormalInvBuildConts")]
		public int EmployerAnnualNormalInvestmentBuilderContributions { get; set; }
		[JsonProperty("memMthNormalInvBuildConts")]
		public int MonthlyNormalInvestmentBuilderContributions { get; set; }
		[JsonProperty("empMthNormalInvBuildConts")]
		public int EmployerMonthlyNormalInvestmentBuilderContributions { get; set; }
		[JsonProperty("mthEmpConts")]
		public int MonthlyEmployerContributions { get; set; }
		[JsonProperty("annEmpConts")]
		public int AnnualEmployerContributions { get; set; }
		[JsonProperty("annMemSavingNi")]
		public int AnnualMemberNationalInsuranceSaving { get; set; }
		[JsonProperty("mthMemSavingNi")]
		public int MonthlyMemberNationalInsuranceSaving { get; set; }
		[JsonProperty("annMemSavingTax")]
		public decimal AnnualTaxSaving { get; set; }
		[JsonProperty("mthMemSavingTax")]
		public decimal MonthlyTaxSaving { get; set; }
		[JsonProperty("annScottishMemSavingTax")]
		public int AnnualScottishTaxSaving { get; set; }
		[JsonProperty("mthScottishMemSavingTax")]
		public int MonthlyScottishTaxSaving { get; set; }
		[JsonProperty("annNetMemberCost")]
		public int AnnualNetCost { get; set; }
		[JsonProperty("mthNetMemberCost")]
		public int MonthlyNetCost { get; set; }
		[JsonProperty("rateSavingRelief")]
		public decimal RateOfSavingsRelief { get; set; }
		[JsonProperty("memAnnStandardSchemeConts")]
		public decimal AnnualStandardSchemeContributions { get; set; }
		[JsonProperty("memMthStandardSchemeConts")]
		public decimal MonthlyStandardSchemeContributions { get; set; }
		[JsonProperty("shouldShowPotentialAnnualAllowanceBreachWarning")]
		public bool AnnualAllowanceBreachWarning { get; set; }
	}
}
