﻿using System;
using Newtonsoft.Json;

namespace USS_API_Tests.Modellers.CostModeller
{
	[JsonObject]
	public class TaxModellerResponse
	{
		[JsonProperty("ussPensionInputAmt")]
		public int UssPensionInputAmount { get; set; }
		[JsonProperty("dbPensionInputAmt")]
		public int DbPensionInputAmount { get; set; }
		[JsonProperty("dcPensionInputAmt")]
		public int DcPensionInputAmount { get; set; }
		[JsonProperty("memEstimatedAnnAllowance")]
		public int MemberEstimatedAnnualAllowance { get; set; }
		[JsonProperty("isSubjectToTaperedAnnAllowance")]
		public bool IsSubjectToTaperedAnnualAllowance { get; set; }
		[JsonProperty("memAdjIncome")]
		public int MemberAdjustedIncome { get; set; }
		[JsonProperty("isExceedingAnnAllowance")]
		public bool IsExceedingAnnualAllowance { get; set; }
		[JsonProperty("memUnusedAnnAllowance")]
		public int MemberUnusedAnnualAllowance { get; set; }
		[JsonProperty("taxableAmt")]
		public int TaxableAmount { get; set; }
		[JsonProperty("dcExcessAmt")]
		public IndexOutOfRangeException DcExcessAmount { get; set; }
		[JsonProperty("dbExcessAmt")]
		public int DbExcessAmount { get; set; }
		[JsonProperty("isExceedingDbAnnualAllowance")]
		public bool IsExceedingDbAnnualAllowance { get; set; }
		[JsonProperty("dcUnusedAnnAllowance")]
		public int DcUnusedAnnualAllowance { get; set; }
		[JsonProperty("isExceedingDCAnnualAllowance")]
		public bool IsExceedingDcAnnualAllowance { get; set; }
		[JsonProperty("dbUnusedAnnAllowance")]
		public int DbUnusedAnnualAllowance { get; set; }
		[JsonProperty("alternativeChargeableAmt")]
		public int AlternativeChargeableAmount { get; set; }
	}
}
