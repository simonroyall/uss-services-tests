﻿using Newtonsoft.Json;

namespace USS_API_Tests.Modellers.CostModeller
{
	[JsonObject]
	public class CostModellerRequest
	{
		[JsonProperty("annualSalary")]
		public int AnnualSalary { get; set; }
		[JsonProperty("additionalContributionValue")]
		public decimal AdditionalContributionValue { get; set; }
		[JsonProperty("additionalContributionType")]
		public string AdditionalContributionType { get; set; }
		[JsonProperty("fsAvcRate")]
		public int FsavcRate { get; set; }
		[JsonProperty("crbAvcAmount")]
		public int CrbAvcAmount { get; set; }
		[JsonProperty("isSalarySacrifice")]
		public bool IsSalarySacrifice { get; set; }
		[JsonProperty("shouldUseScottishTaxBrackets")]
		public bool ShouldUseScottishTaxBrackets { get; set; }

	}
}
