﻿using Newtonsoft.Json;

namespace USS_API_Tests.Modellers.CostModeller
{
	[JsonObject]
	public class TaxModellerRequest
	{
		[JsonProperty("annualSalary")]
		public int AnnualSalary { get; set; }
		[JsonProperty("invBuildLSAVC")]
		public int InvInvestmentBuilderLSAVC { get; set; }
		[JsonProperty("memAnnNormalInvBuildConts")]
		public int MemberAnnualNormalInvestmentBuilderContributions { get; set; }
		[JsonProperty("empAnnNormalInvBuildConts")]
		public int EmployerAnnualNormalInvestmentBuilderContributions { get; set; }
		[JsonProperty("memAnnAddInvBuildConts")]
		public int MemberAnnualAdditionalInvestmentBuilderContributions { get; set; }
		[JsonProperty("memAnnSchemeConts")]
		public int MemberAnnualSchemeContributions { get; set; }
		[JsonProperty("isSalarySacrifice")]
		public bool IsSalarySacrifice { get; set; }
		[JsonProperty("addTaxableIncome")]
		public int AdditionalTaxableIncome { get; set; }
		[JsonProperty("mpaaIndicator")]
		public bool MpaaIndicator { get; set; }
	}
}
