﻿namespace USS_API_Tests.Modellers.CostModeller
{
	public class CostModellerTestConfigItem: CostModellerRequest
	{
		public int ExpectedMemberMonthlySchemeContributions { get; set; }
		public int ExpectedAnnualSchemeContributions { get; set; }
		public int ExpectedMemberMonthlyAdditionalInvestmentBuilderContributions { get; set; }
		public int ExpectedMemberAnnualAdditionalInvestmentBuilderContributions { get; set; }
		public int ExpectedMemberAnnualNormalInvestmentBuilderContributions { get; set; }
		public int ExpectedEmployerAnnualNormalInvestmentBuilderContributions { get; set; }
		public int ExpectedMemberMonthlyNormalInvestmentBuilderContributions { get; set; }
		public int ExpectedEmployerMonthlyNormalInvestmentBuilderContributions { get; set; }
		public int ExpectedAnnualScottishMemberSavingTax { get; set; }
		public int ExpectedMonthlyScottishMemberSavingTax { get; set; }
		public decimal ExpectedAnnualMemberSavingTax { get; set; }
		public decimal ExpectedMonthlyMemberSavingTax { get; set; }
		public int ExpectedMonthlyMemberSavingNationalInsurance { get; set; }
		public int ExpectedAnnualMemberSavingNationalInsurance { get; set; }
		public int ExpectedMonthlyEmployerContributions { get; set; }
		public int ExpectedAnnualEmployerContributions { get; set; }
		public int ExpectedAnnualNetMemberCost { get; set; }
		public int ExpectedMonthlyNetMemberCost { get; set; }
		public decimal ExpectedRateSavingRelief { get; set; }
		public decimal ExpectedMemberAnnualStandardSchemeContributions { get; set; }
		public decimal ExpectedMemberMonthlyStandardSchemeContributions { get; set; }
		public bool ExpectedAnnualAllowanceBreachWarning { get; set; }
		public string TestName { get; set; }
	}
}