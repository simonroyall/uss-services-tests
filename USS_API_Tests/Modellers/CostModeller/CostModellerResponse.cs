﻿using System.Collections.Generic;
using Newtonsoft.Json;
using USS_API_Tests.Modellers.AdditionalContributions;

namespace USS_API_Tests.Modellers.CostModeller
{
	public class CostModellerResponse
	{
		public CostModellerResponseData Data { get; set; }
		public IEnumerable<string> Errors { get; set; }

	}
}
