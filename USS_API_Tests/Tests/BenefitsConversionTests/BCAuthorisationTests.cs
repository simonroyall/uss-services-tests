﻿using System;
using System.Configuration;
using System.Net;
using NUnit.Framework;
using RestSharp;
using USS_API_Tests.Modellers.BenefitsConversion;

namespace USS_API_Tests.Tests.BenefitsConversionTests
{
	[TestFixture]
	class BCModellerAuthorisationTests: BenefitsConversionModellerBaseTest
	{
		public override RestRequest InitialiseRequest(BenefitsConversionRequest requestPayload)
		{
			var request = new RestRequest("benefitsconversion", Method.POST);
			string badKey = "a1ea0cdb656e43a484549c77b3f7ff09";
			request.AddJsonBody(requestPayload);
			request.AddHeader("Ocp-Apim-Subscription-Key", badKey	);
			return request;
		}

		[Test]
		[Category("API Benefits Conversion")]
		[Category("API Authorisation")]
		[TestCase(TestName = "No Access Given Invalid APIM Key")]
		public void Given_InvalidAPIMKey_Get_ErrorMessage()
		{
			var serviceHost = ConfigurationManager.AppSettings[$"{Environment()}.Service.Host"];

			var requestPayload = new BenefitsConversionRequest
			{
				DateOfBirth = new DateTime(10, 10, 1980),
				DateOfRetirement = new DateTime(10, 10, 2054),
				PreCommutationPensionAmount = 1000,
				InvestmentBuilderNonPruAmount = 100,
				InvestmentBuilderPruOnlyAmount = 100,
				PruMpavcFundAmount = 100,
				FutureLumpSumPaymentAmount = 100,
				
			};

			var restClient = InitialiseRestClient(serviceHost);
			var request = InitialiseRequest(requestPayload);

			var apiResponse = restClient.Execute<BenefitsConversionResponse>(request);
			

			Assert.AreEqual(HttpStatusCode.Unauthorized, apiResponse.StatusCode, $"service was accessible despite invalid key");

			
		}
	}
}
