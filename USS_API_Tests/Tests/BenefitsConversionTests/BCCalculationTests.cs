﻿using System;
using System.Configuration;
using System.Net;
using NUnit.Framework;
using USS_API_Tests.Modellers.BenefitsConversion;

namespace USS_API_Tests.Tests.BenefitsConversionTests
{
	[TestFixture]
	class BcCalculationTests: BenefitsConversionModellerBaseTest
	{

		[Test]
		[TestCaseSource(nameof(GetTestData))]
		[Category("API Benefits Conversion")]
		[Category("API Calculation")]
		public void VerifyValidData(BenefitsConversionTestConfigItem testItem)
		{

			var serviceHost = ConfigurationManager.AppSettings[$"{Environment()}.Service.Host"];
											

			var requestPayload = new BenefitsConversionRequest
			{
				DateOfBirth = new DateTime(10, 10, 1980),
				DateOfRetirement = new DateTime(10, 10, 2054),
				PreCommutationPensionAmount = 1000,
				InvestmentBuilderNonPruAmount = 100,
				InvestmentBuilderPruOnlyAmount = 100,
				PruMpavcFundAmount = 100,
				FutureLumpSumPaymentAmount = 100,
			};

			var restClient = InitialiseRestClient(serviceHost);
			var request = InitialiseRequest(requestPayload);

			var apiResponse = restClient.Execute<BenefitsConversionResponse>(request);
			if (apiResponse.Data?.Errors != null)
			{
				var errors = string.Join(", ", apiResponse.Data?.Errors);
			
				Assert.AreEqual(HttpStatusCode.OK, apiResponse.StatusCode, $"test failed with errors {errors}");
			}

			var responseData = apiResponse.Data;

			Assert.IsNotNull(responseData);
			
			Assert.That(testItem.ExpectedMinumumPension, Is.EqualTo(responseData.MinPensionAmount).Within(2), "Minimum Pension Amount");
			Assert.That(testItem.ExpectedMaximumPension, Is.EqualTo(responseData.MaxPensionAmount).Within(2), "Maximum Pension Amount");			
			Assert.That(testItem.ExpectedMinimumCash, Is.EqualTo(responseData.MaxCashAmount).Within(2), "Max Tax Free Cash");
			Assert.That(testItem.ExpectedMaxUncrystallised, Is.EqualTo(responseData.MaxUncrystalisedAmount).Within(1).Percent, "Maximum uncrystallised");						

		}
		
	}
}
