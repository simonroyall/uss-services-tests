﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using RestSharp;
using USS_API_Tests.Modellers.BenefitsConversion;
using USS_API_Tests.Serialization;

namespace USS_API_Tests.Tests.BenefitsConversionTests
{
	public abstract class BenefitsConversionModellerBaseTest
	{
		public virtual RestRequest InitialiseRequest(BenefitsConversionRequest requestPayload)
		{
			var request = new RestRequest("additionalcontributions", Method.POST);
			request.AddJsonBody(requestPayload);
			request.AddHeader("Ocp-Apim-Subscription-Key", ConfigurationManager.AppSettings[$"{Environment()}.Apim.Subscription.Key"]);
			return request;
		}
		public static RestClient InitialiseRestClient(string serviceHost)
		{
			var restClient = new RestClient(serviceHost);
			restClient.UseSerializer(() => new JsonNetSerializer());
			return restClient;
		}

		public string Environment()
		{
			var where = ConfigurationManager.AppSettings["Environment"];
			IList<string> LegitimateEnvironments = new List<string>();
			LegitimateEnvironments.Add("Test");
			LegitimateEnvironments.Add("UAT");
			LegitimateEnvironments.Add("Live");

			if (LegitimateEnvironments.Contains(where))
				return @where;
			throw new Exception($"'{@where}' is not a legitimate Environment - check the Config file!");
		}

		public static IEnumerable<BenefitsConversionTestConfigItem> GetTestData()
		{

			var testSource = File.ReadAllText(Path.Combine(Directory.GetCurrentDirectory(), $"\\Projects\\USSServices\\USS_API_Tests\\Data\\bencon.api.data.json"));

			var testItems = JsonConvert.DeserializeObject<IEnumerable<BenefitsConversionTestConfigItem>>(testSource);

			if (IsInLive)
			{
				return testItems.Take(5);
			}
			return testItems;
		}

		static bool IsInLive => ConfigurationManager.AppSettings["Environment"]=="Live";
		
	}
}
