﻿using System;
using System.Configuration;
using System.Linq;
using System.Net;
using NUnit.Framework;
using USS_API_Tests.Modellers.AdditionalContributions;

namespace USS_API_Tests.Tests.AdditionalContributionsTests
{
	[TestFixture]
    public class ValidationTests: AdditionalContributionsModellerBaseTest
    {

                
		[Test]			
		[Category("API Additional Contributions")]
		[Category("API Validation")]
		[TestCase("4007",TestName ="Correct Error Given TRA too old")]
		[TestCase("4007", TestName = "Correct Error Given TRA too young")]
		public void Given_InvalidTRA_Get_ErrorMessage(string expectedErrorCode)
		{
			var serviceHost = ConfigurationManager.AppSettings[$"{Environment()}.Service.Host"];

			int AdjustedTRA()
			{
				if (TestContext.CurrentContext.Test.Name.Contains("young"))
				{
					int boundaryValue = Convert.ToInt16(ConfigurationManager.AppSettings[$"{Environment()}.Min.TRA"]);
					return boundaryValue - 1;
				}
				else if (TestContext.CurrentContext.Test.Name.Contains("old"))
				{
					int boundaryValue = Convert.ToInt16(ConfigurationManager.AppSettings[$"{Environment()}.Max.TRA"]);
					return boundaryValue + 1;
				}
				else throw new Exception("invalid TRA passed - check that the test name contains 'young' or 'old'");
			}
			var requestPayload = new AdditionalContributionsRequest
			{
				AnnualSalary = 89000,
				LumpSumContribution = 10,
				AdditionalContribution = 5,
				DateOfBirth = new DateTime(1970, 06, 01),
				Gender = "m",
				HasVoluntarySalaryCap = false,
				RetirementAge = AdjustedTRA(),
				SalaryIncrease = 2.5,
				VoluntarySalaryCapAmount = 0,
				InflationIncrease = 2.5,
				InvestmentReturn = 5,
			};

			var restClient = InitialiseRestClient(serviceHost);
			var request = InitialiseRequest(requestPayload);

			var apiResponse = restClient.Execute<AdditionalContributionsResponse>(request);
			var errors = string.Join(", ", apiResponse.Data?.Errors);
			
			Assert.AreEqual(HttpStatusCode.BadRequest, apiResponse.StatusCode, $"service accepted TRA of {AdjustedTRA()}");

			var responseData = apiResponse.Data;

			Assert.IsNotNull(responseData);
			Assert.AreEqual(1, responseData.Errors.Count(),$"these errors returned: {errors}");
			Assert.True(errors.Contains(expectedErrorCode),$"error code {expectedErrorCode} expected for TRA of {AdjustedTRA()}");
		}

		[Test]
		[Category("API Additional Contributions")]
		[Category("API Validation")]
		[TestCase("4002", TestName = "Correct Error Given Annual Salary too high")]
		public void Given_InvalidAnnualSalary_Get_ErrorMessage(string expectedErrorCode)
		{
			var serviceHost = ConfigurationManager.AppSettings[$"{Environment()}.Service.Host"];

			decimal AdjustedAnnualSalary()
			{
				if (TestContext.CurrentContext.Test.Name.Contains("low"))
				{
					decimal boundaryValue = Convert.ToDecimal(ConfigurationManager.AppSettings[$"{Environment()}.Min.AnnualSalary"]);
					return boundaryValue - 0.01m;
				}
				else if (TestContext.CurrentContext.Test.Name.Contains("high"))
				{
					decimal boundaryValue = Convert.ToDecimal(ConfigurationManager.AppSettings[$"{Environment()}.Max.AnnualSalary"]);
					return boundaryValue + 0.01m;
				}
				else throw new Exception("invalid Annual salary passed - check that the test name contains 'young' or 'old'");
			}
			var requestPayload = new AdditionalContributionsRequest
			{
				AnnualSalary = AdjustedAnnualSalary(),
				LumpSumContribution = 10,
				AdditionalContribution = 5,
				DateOfBirth = new DateTime(1970, 06, 01),
				Gender = "m",
				HasVoluntarySalaryCap = false,
				RetirementAge = 65,
				SalaryIncrease = 2.5,
				VoluntarySalaryCapAmount = 0,
				InflationIncrease = 2.5,
				InvestmentReturn = 5,
			};

			var restClient = InitialiseRestClient(serviceHost);
			var request = InitialiseRequest(requestPayload);

			var apiResponse = restClient.Execute<AdditionalContributionsResponse>(request);
			var errors = string.Join(", ", apiResponse.Data?.Errors);

			Assert.AreEqual(HttpStatusCode.BadRequest, apiResponse.StatusCode, $"service accepted Annual Salary of {AdjustedAnnualSalary()}");

			var responseData = apiResponse.Data;

			Assert.IsNotNull(responseData);
			Assert.AreEqual(1, responseData.Errors.Count(), $"these errors returned: {errors}");
			Assert.True(errors.Contains(expectedErrorCode), $"error code {expectedErrorCode} expected for Annual Salary of {AdjustedAnnualSalary()}");
		}

		[Test]
		[Category("API Additional Contributions")]
		[Category("API Validation")]
		[TestCase("4001", TestName = "Correct Error Given DOB in future")]
		public void Given_DOB_InFuture_Get_ErrorMessage(string expectedErrorCode)
		{
			var serviceHost = ConfigurationManager.AppSettings[$"{Environment()}.Service.Host"];

			
			var requestPayload = new AdditionalContributionsRequest
			{
				AnnualSalary = 50000,
				LumpSumContribution = 1000,
				AdditionalContribution = 5,
				DateOfBirth = DateTime.Now.AddDays(1),
				Gender = "m",
				HasVoluntarySalaryCap = false,
				RetirementAge = 65,
				SalaryIncrease = 2.5,
				VoluntarySalaryCapAmount = 0,
				InflationIncrease = 2.5,
				InvestmentReturn = 5,
			};

			var restClient = InitialiseRestClient(serviceHost);
			var request = InitialiseRequest(requestPayload);

			var apiResponse = restClient.Execute<AdditionalContributionsResponse>(request);
			var errors = string.Join(", ", apiResponse.Data?.Errors);

			Assert.AreEqual(HttpStatusCode.BadRequest, apiResponse.StatusCode, $"service accepted DOB of {requestPayload.DateOfBirth.ToString()}");

			var responseData = apiResponse.Data;

			Assert.IsNotNull(responseData);
			Assert.AreEqual(2, responseData.Errors.Count(), $"these errors returned: {errors}");
			Assert.True(errors.Contains(expectedErrorCode), $"error code {expectedErrorCode} expected for DOB of {requestPayload.DateOfBirth.ToString()}");
		}

		[Test]
		[Category("API Additional Contributions")]
		[Category("API Validation")]
		[TestCase("4003", TestName = "Correct Error Given Salary Cap exceeding Salary")]
		public void Given_SalaryCap_Exceeds_Salary_Get_ErrorMessage(string expectedErrorCode)
		{
			var serviceHost = ConfigurationManager.AppSettings[$"{Environment()}.Service.Host"];


			var requestPayload = new AdditionalContributionsRequest
			{
				AnnualSalary = 50000,
				LumpSumContribution = 1000,
				AdditionalContribution = 5,
				DateOfBirth = new DateTime(1970,06,01),
				Gender = "m",
				HasVoluntarySalaryCap = true,				
				RetirementAge = 65,
				SalaryIncrease = 2.5,
				VoluntarySalaryCapAmount = 50001,
				InflationIncrease = 2.5,
				InvestmentReturn = 5,
			};

			var restClient = InitialiseRestClient(serviceHost);
			var request = InitialiseRequest(requestPayload);

			var apiResponse = restClient.Execute<AdditionalContributionsResponse>(request);
			var errors = string.Join(", ", apiResponse.Data?.Errors);

			Assert.AreEqual(HttpStatusCode.BadRequest, apiResponse.StatusCode, $"service accepted voluntary salary cap in excess of annual salary");

			var responseData = apiResponse.Data;

			Assert.IsNotNull(responseData);
			Assert.AreEqual(1, responseData.Errors.Count(), $"these errors returned: {errors}");
			Assert.True(errors.Contains(expectedErrorCode), $"error code {expectedErrorCode} expected for voluntary salary cap greater than annual salary");
		}

		[Test]
		[Category("API Additional Contributions")]
		[Category("API Validation")]
		[TestCase("4029", TestName = "Correct Error Given Salary Cap under threshold")]
		public void Given_SalaryCap_Under_SalaryThreshold_Get_ErrorMessage(string expectedErrorCode)
		{
			var serviceHost = ConfigurationManager.AppSettings[$"{Environment()}.Service.Host"];

			decimal GetBadSalaryCapAmount()
			{
				return Convert.ToDecimal(ConfigurationManager.AppSettings[$"{Environment()}.SalaryThreshold"])-0.01m;
			}
			

			var requestPayload = new AdditionalContributionsRequest
			{
				AnnualSalary = 50000,
				LumpSumContribution = 1000,
				AdditionalContribution = 5,
				DateOfBirth = new DateTime(1970, 06, 01),
				Gender = "m",
				HasVoluntarySalaryCap = true,
				RetirementAge = 65,
				SalaryIncrease = 2.5,
				VoluntarySalaryCapAmount = GetBadSalaryCapAmount(),
				InflationIncrease = 2.5,
				InvestmentReturn = 5,
			};

			var restClient = InitialiseRestClient(serviceHost);
			var request = InitialiseRequest(requestPayload);

			var apiResponse = restClient.Execute<AdditionalContributionsResponse>(request);
			var errors = string.Join(", ", apiResponse.Data?.Errors);

			Assert.AreEqual(HttpStatusCode.BadRequest, apiResponse.StatusCode, $"service accepted voluntary salary cap below threshold");

			var responseData = apiResponse.Data;

			Assert.IsNotNull(responseData);
			Assert.AreEqual(1, responseData.Errors.Count(), $"these errors returned: {errors}");
			Assert.True(errors.Contains(expectedErrorCode), $"error code {expectedErrorCode} expected for voluntary salary cap below threshold");
		}

		[Test]
		[Category("API Additional Contributions")]
		[Category("API Validation")]
		[TestCase("4004", TestName = "Correct Error Given Add Contrib too small")]
		[TestCase("4004", TestName = "Correct Error Given Add Contrib too big")]
		public void Given_InvalidAdditionalContribution_Get_ErrorMessage(string expectedErrorCode)
		{
			var serviceHost = ConfigurationManager.AppSettings[$"{Environment()}.Service.Host"];

			double GetBadAdditionalContribution()
			{
				if (TestContext.CurrentContext.Test.Name.Contains("small"))
				{
					double boundaryValue = Convert.ToInt16(ConfigurationManager.AppSettings[$"{Environment()}.Min.AdditionalContributions"]);
					return boundaryValue - 0.1;
				}
				else if (TestContext.CurrentContext.Test.Name.Contains("big"))
				{
					double boundaryValue = Convert.ToInt16(ConfigurationManager.AppSettings[$"{Environment()}.Max.AdditionalContributions"]);
					return boundaryValue + 0.1;
				}
				else throw new Exception("invalid Additional Contribution passed - check that the test name contains 'small' or 'big'");
			}
			var requestPayload = new AdditionalContributionsRequest
			{
				AnnualSalary = 89000,
				LumpSumContribution = 10,
				AdditionalContribution = GetBadAdditionalContribution(),
				DateOfBirth = new DateTime(1970, 06, 01),
				Gender = "m",
				HasVoluntarySalaryCap = false,
				RetirementAge = 65,
				SalaryIncrease = 2.5,
				VoluntarySalaryCapAmount = 0,
				InflationIncrease = 2.5,
				InvestmentReturn = 5,
			};

			var restClient = InitialiseRestClient(serviceHost);
			var request = InitialiseRequest(requestPayload);

			var apiResponse = restClient.Execute<AdditionalContributionsResponse>(request);
			var errors = string.Join(", ", apiResponse.Data?.Errors);

			Assert.AreEqual(HttpStatusCode.BadRequest, apiResponse.StatusCode, $"service accepted Additional Contribution of {GetBadAdditionalContribution()}");

			var responseData = apiResponse.Data;

			Assert.IsNotNull(responseData);
			Assert.AreEqual(1, responseData.Errors.Count(), $"these errors returned: {errors}");
			Assert.True(errors.Contains(expectedErrorCode), $"error code {expectedErrorCode} expected for Additional Contribution of {GetBadAdditionalContribution()}");
		}

		[Test]
		[Category("API Additional Contributions")]
		[Category("API Validation")]
		[TestCase("4005", TestName = "Correct Error Given Current Age too young")]
		[TestCase("4005", TestName = "Correct Error Given Current Age too old")]
		public void Given_Age_OutOfRange_Get_ErrorMessage(string expectedErrorCode)
		{
			var serviceHost = ConfigurationManager.AppSettings[$"{Environment()}.Service.Host"];

			DateTime GetInvalidAge()
			{
				if (TestContext.CurrentContext.Test.Name.Contains("young"))
				{
					DateTime SixteenYearsAgo = DateTime.Now.AddYears(-16);
					return SixteenYearsAgo.AddDays(1);
				}
				else if (TestContext.CurrentContext.Test.Name.Contains("old"))
				{
					return DateTime.Now.AddYears(-76);					
				}
				else throw new Exception("invalid DOB passed - check that the test name contains 'young' or 'old'");
			}
			var requestPayload = new AdditionalContributionsRequest
			{
				AnnualSalary = 89000,
				LumpSumContribution = 10,
				AdditionalContribution = 5,
				DateOfBirth = GetInvalidAge(),
				Gender = "m",
				HasVoluntarySalaryCap = false,
				RetirementAge = 75,
				SalaryIncrease = 2.5,
				VoluntarySalaryCapAmount = 0,
				InflationIncrease = 2.5,
				InvestmentReturn = 5,
			};

			var restClient = InitialiseRestClient(serviceHost);
			var request = InitialiseRequest(requestPayload);

			var apiResponse = restClient.Execute<AdditionalContributionsResponse>(request);
			var errors = string.Join(", ", apiResponse.Data?.Errors);

			Assert.AreEqual(HttpStatusCode.BadRequest, apiResponse.StatusCode, $"service accepted DOB of {GetInvalidAge()}");

			var responseData = apiResponse.Data;

			Assert.IsNotNull(responseData);
			Assert.AreEqual(1, responseData.Errors.Count(), $"these errors returned: {errors}");
			Assert.True(errors.Contains(expectedErrorCode), $"error code {expectedErrorCode} expected for DOB of {GetInvalidAge().ToShortDateString()}");
		}

		[Test]
		[Category("API Additional Contributions")]
		[Category("API Validation")]
		[TestCase("4006", TestName = "Correct Error Given Lump Sum exceeds Salary")]
		public void Given_Lump_Exceeds_Salary_Get_ErrorMessage(string expectedErrorCode)
		{
			var serviceHost = ConfigurationManager.AppSettings[$"{Environment()}.Service.Host"];


			var requestPayload = new AdditionalContributionsRequest
			{
				AnnualSalary = 50000,
				LumpSumContribution = 50000.01m,
				AdditionalContribution = 5,
				DateOfBirth = new DateTime(1970, 06, 01),
				Gender = "m",
				HasVoluntarySalaryCap = false,
				RetirementAge = 65,
				SalaryIncrease = 2.5,
				VoluntarySalaryCapAmount = 30000,
				InflationIncrease = 2.5,
				InvestmentReturn = 5,
			};

			var restClient = InitialiseRestClient(serviceHost);
			var request = InitialiseRequest(requestPayload);

			var apiResponse = restClient.Execute<AdditionalContributionsResponse>(request);
			var errors = string.Join(", ", apiResponse.Data?.Errors);

			Assert.AreEqual(HttpStatusCode.BadRequest, apiResponse.StatusCode, $"service accepted lump sum in excess of annual salary");

			var responseData = apiResponse.Data;

			Assert.IsNotNull(responseData);			
			Assert.AreEqual(1, responseData.Errors.Count(), $"these errors returned: {errors}");
			Assert.True(errors.Contains(expectedErrorCode), $"error code {expectedErrorCode} expected for lump sum greater than annual salary");
		}

		[Test]
		[Category("API Additional Contributions")]
		[Category("API Validation")]
		[TestCase("4027", TestName = "Correct Error Given Retirement Date in past")]	
		public void Given_TRA__LessThan_CurrentAge_Get_ErrorMessage(string expectedErrorCode)
		{
			var serviceHost = ConfigurationManager.AppSettings[$"{Environment()}.Service.Host"];
			int RetirementAge = 60;
			
	
			DateTime GetDOBMakingMemberOlderThanTRA()
			{
				DateTime DOBForTRA = DateTime.Now.AddYears(-RetirementAge);
				return DOBForTRA.AddDays(-364);
			}

			var requestPayload = new AdditionalContributionsRequest
			{
				AnnualSalary = 89000,
				LumpSumContribution = 10,
				AdditionalContribution = 5,
				DateOfBirth = GetDOBMakingMemberOlderThanTRA(),
				Gender = "m",
				HasVoluntarySalaryCap = false,
				RetirementAge = RetirementAge,
				SalaryIncrease = 2.5,
				VoluntarySalaryCapAmount = 0,
				InflationIncrease = 2.5,
				InvestmentReturn = 5,
			};

			var restClient = InitialiseRestClient(serviceHost);
			var request = InitialiseRequest(requestPayload);

			var apiResponse = restClient.Execute<AdditionalContributionsResponse>(request);
			var errors = string.Join(", ", apiResponse.Data?.Errors);

			Assert.AreEqual(HttpStatusCode.BadRequest, apiResponse.StatusCode, $"service accepted retirement date in the past");

			var responseData = apiResponse.Data;

			Assert.IsNotNull(responseData);
			Assert.AreEqual(1, responseData.Errors.Count(), $"these errors returned: {errors}");
			Assert.True(errors.Contains(expectedErrorCode), $"error code {expectedErrorCode} expected for current age > retirement age");
		}

		[Test]
		[Category("API Additional Contributions")]
		[Category("API Validation")]
		[TestCase("4008", TestName = "Correct Error Given Investment Return too low")]
		[TestCase("4008", TestName = "Correct Error Given Investment Return too high")]
		public void Given_InvalidInvestmentReturn_Get_ErrorMessage(string expectedErrorCode)
		{
			var serviceHost = ConfigurationManager.AppSettings[$"{Environment()}.Service.Host"];

			double GetInvalidInvestmentReturn()
			{
				if (TestContext.CurrentContext.Test.Name.Contains("low"))
				{
					double boundaryValue = Convert.ToDouble(ConfigurationManager.AppSettings[$"{Environment()}.Min.InvestmentReturn"]);
					return boundaryValue - 0.1;
				}
				else if (TestContext.CurrentContext.Test.Name.Contains("high"))
				{
					double boundaryValue = Convert.ToDouble(ConfigurationManager.AppSettings[$"{Environment()}.Max.InvestmentReturn"]);
					return boundaryValue + 0.1;
				}
				else throw new Exception("invalid investment return passed - check that the test name contains 'high' or 'low'");
			}
			var requestPayload = new AdditionalContributionsRequest
			{
				AnnualSalary = 50000,
				LumpSumContribution = 10,
				AdditionalContribution = 5,
				DateOfBirth = new DateTime(1970, 06, 01),
				Gender = "m",
				HasVoluntarySalaryCap = false,
				RetirementAge = 65,
				SalaryIncrease = 2.5,
				VoluntarySalaryCapAmount = 0,
				InflationIncrease = 2.5,
				InvestmentReturn = GetInvalidInvestmentReturn(),
			};

			var restClient = InitialiseRestClient(serviceHost);
			var request = InitialiseRequest(requestPayload);

			var apiResponse = restClient.Execute<AdditionalContributionsResponse>(request);
			var errors = string.Join(", ", apiResponse.Data?.Errors);

			Assert.AreEqual(HttpStatusCode.BadRequest, apiResponse.StatusCode, $"service accepted Investment Return of {GetInvalidInvestmentReturn()}");

			var responseData = apiResponse.Data;

			Assert.IsNotNull(responseData);
			Assert.AreEqual(1, responseData.Errors.Count(), $"these errors returned: {errors}");
			Assert.True(errors.Contains(expectedErrorCode), $"error code {expectedErrorCode} expected for Investment Return of {GetInvalidInvestmentReturn()}");
		}

		[Test]
		[Category("API Additional Contributions")]
		[Category("API Validation")]
		[TestCase("4009", TestName = "Correct Error Given Inflation Increase too low")]
		[TestCase("4009", TestName = "Correct Error Given Inflation Increase too high")]
		public void Given_InvalidInflationIncrease_Get_ErrorMessage(string expectedErrorCode)
		{
			var serviceHost = ConfigurationManager.AppSettings[$"{Environment()}.Service.Host"];

			double GetInvalidInflationIncrease()
			{
				if (TestContext.CurrentContext.Test.Name.Contains("low"))
				{
					double boundaryValue = Convert.ToDouble(ConfigurationManager.AppSettings[$"{Environment()}.Min.InflationIncrease"]);
					return boundaryValue - 0.1;
				}
				else if (TestContext.CurrentContext.Test.Name.Contains("high"))
				{
					double boundaryValue = Convert.ToDouble(ConfigurationManager.AppSettings[$"{Environment()}.Max.InflationIncrease"]);
					return boundaryValue + 0.1;
				}
				else throw new Exception("invalid inflation increase passed - check that the test name contains 'high' or 'low'");
			}
			var requestPayload = new AdditionalContributionsRequest
			{
				AnnualSalary = 50000,
				LumpSumContribution = 10,
				AdditionalContribution = 5,
				DateOfBirth = new DateTime(1970, 06, 01),
				Gender = "m",
				HasVoluntarySalaryCap = false,
				RetirementAge = 65,
				SalaryIncrease = 2.5,
				VoluntarySalaryCapAmount = 0,
				InflationIncrease = GetInvalidInflationIncrease(),
				InvestmentReturn = 2.5,
			};

			var restClient = InitialiseRestClient(serviceHost);
			var request = InitialiseRequest(requestPayload);

			var apiResponse = restClient.Execute<AdditionalContributionsResponse>(request);
			var errors = string.Join(", ", apiResponse.Data?.Errors);

			Assert.AreEqual(HttpStatusCode.BadRequest, apiResponse.StatusCode, $"service accepted Inflation Increase of {GetInvalidInflationIncrease()}");

			var responseData = apiResponse.Data;

			Assert.IsNotNull(responseData);
			Assert.AreEqual(1, responseData.Errors.Count(), $"these errors returned: {errors}");
			Assert.True(errors.Contains(expectedErrorCode), $"error code {expectedErrorCode} expected for Inflation Increase of {GetInvalidInflationIncrease()}");
		}

		[Test]
		[Category("API Additional Contributions")]
		[Category("API Validation")]
		[TestCase("4010", TestName = "Correct Error Given Salary Increase too low")]
		[TestCase("4010", TestName = "Correct Error Given Salary Increase too high")]
		public void Given_InvalidSalaryIncrease_Get_ErrorMessage(string expectedErrorCode)
		{
			var serviceHost = ConfigurationManager.AppSettings[$"{Environment()}.Service.Host"];

			double GetInvalidSalaryIncrease()
			{
				if (TestContext.CurrentContext.Test.Name.Contains("low"))
				{
					double boundaryValue = Convert.ToDouble(ConfigurationManager.AppSettings[$"{Environment()}.Min.SalaryIncrease"]);
					return boundaryValue - 0.1;
				}
				else if (TestContext.CurrentContext.Test.Name.Contains("high"))
				{
					double boundaryValue = Convert.ToDouble(ConfigurationManager.AppSettings[$"{Environment()}.Max.SalaryIncrease"]);
					return boundaryValue + 0.1;
				}
				else throw new Exception("invalid salary increase passed - check that the test name contains 'high' or 'low'");
			}
			var requestPayload = new AdditionalContributionsRequest
			{
				AnnualSalary = 50000,
				LumpSumContribution = 10,
				AdditionalContribution = 5,
				DateOfBirth = new DateTime(1970, 06, 01),
				Gender = "m",
				HasVoluntarySalaryCap = false,
				RetirementAge = 65,
				SalaryIncrease = GetInvalidSalaryIncrease(),
				VoluntarySalaryCapAmount = 0,
				InflationIncrease = 2.5,
				InvestmentReturn = 2.5,
			};

			var restClient = InitialiseRestClient(serviceHost);
			var request = InitialiseRequest(requestPayload);

			var apiResponse = restClient.Execute<AdditionalContributionsResponse>(request);
			var errors = string.Join(", ", apiResponse.Data?.Errors);

			Assert.AreEqual(HttpStatusCode.BadRequest, apiResponse.StatusCode, $"service accepted Salary Increase of {GetInvalidSalaryIncrease()}");

			var responseData = apiResponse.Data;

			Assert.IsNotNull(responseData);
			Assert.AreEqual(1, responseData.Errors.Count(), $"these errors returned: {errors}");
			Assert.True(errors.Contains(expectedErrorCode), $"error code {expectedErrorCode} expected for Salary Increase of {GetInvalidSalaryIncrease()}");
		}

	}
}
