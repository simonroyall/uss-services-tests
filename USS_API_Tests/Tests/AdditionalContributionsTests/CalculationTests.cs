﻿using System.Collections.Generic;
using System.Configuration;
using System.Net;
using NUnit.Framework;
using USS_API_Tests.Modellers.AdditionalContributions;

namespace USS_API_Tests.Tests.AdditionalContributionsTests
{
	[TestFixture]
	class CalculationTests: AdditionalContributionsModellerBaseTest
	{
		private static List<object> _testData = new List<object>();

		[Test]
		[TestCaseSource(nameof(GetTestData))]
		[Category("API Additional Contributions")]
		[Category("API Calculation")]
		public void VerifyValidData(AdditionalContributionsTestConfigItem testItem)
		{

			var serviceHost = ConfigurationManager.AppSettings[$"{Environment()}.Service.Host"];
								
			var adjustedDOB = testItem.DOBOriginal.AddMonths(2);

			var requestPayload = new AdditionalContributionsRequest
			{
				AnnualSalary = testItem.Salary,
				LumpSumContribution = testItem.LumpSumAVC,
				AdditionalContribution = testItem.AVCPercent * 100,
				DateOfBirth = adjustedDOB,
				Gender = testItem.Sex,
				HasVoluntarySalaryCap = testItem.VSC > 0 ? true : false,
				InvestmentReturn = testItem.InvestmentReturn * 100,
				RetirementAge = testItem.RetAge,
				SalaryIncrease = testItem.SalaryIncrease * 100,
				VoluntarySalaryCapAmount = testItem.VSC,
				InflationIncrease = testItem.Inflation * 100,
			};

			var restClient = InitialiseRestClient(serviceHost);
			var request = InitialiseRequest(requestPayload);

			var apiResponse = restClient.Execute<AdditionalContributionsResponse>(request);
			var errors = string.Join(", ", apiResponse.Data?.Errors);
			
			Assert.AreEqual(HttpStatusCode.OK, apiResponse.StatusCode, $"test failed with errors {errors}");

			var responseData = apiResponse.Data;

			Assert.IsNotNull(responseData);
			
			Assert.That(testItem.ExpectedYearlyRetirementIncome, Is.EqualTo(responseData.Data.AnnualPensionAmount).Within(2), "Annual Pension Amount");
			Assert.That(testItem.ExpectedAnnualAdditionalContributions, Is.EqualTo(responseData.Data.AdditionalContributions).Within(2), " Additional Contributions");			
			Assert.That(testItem.ExpectedLumpSum, Is.EqualTo(responseData.Data.LumpSumContributions).Within(2), "Lump Sum Contributions");
			Assert.That(testItem.ExpectedRetirementPot, Is.EqualTo(responseData.Data.TodayFundValue).Within(1).Percent, "Fund Value in today's money");			
			Assert.That(testItem.ExpectedTaxRelief, Is.EqualTo(responseData.Data.TaxSavings).Within(2), "Tax savings");			
			Assert.That(testItem.ExpectedTotalAdditionalAmount, Is.EqualTo(responseData.Data.TotalCombinedContributions).Within(2), "total combined contributions");			
			Assert.That(testItem.ExpectedCostToMember, Is.EqualTo(responseData.Data.CostToMember).Within(2),"cost to member");

		}
		
	}
}
