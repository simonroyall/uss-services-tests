﻿using System;
using System.Configuration;
using System.Net;
using NUnit.Framework;
using RestSharp;
using USS_API_Tests.Modellers.AdditionalContributions;

namespace USS_API_Tests.Tests.AdditionalContributionsTests
{
	[TestFixture]
	class ACModellerAuthorisationTests: AdditionalContributionsModellerBaseTest
	{
		public override RestRequest InitialiseRequest(AdditionalContributionsRequest requestPayload)
		{
			var request = new RestRequest("additionalcontributions", Method.POST);
			string badKey = "a1ea0cdb656e43a484549c77b3f7ff09";
			request.AddJsonBody(requestPayload);
			request.AddHeader("Ocp-Apim-Subscription-Key", badKey	);
			return request;
		}

		[Test]
		[Category("API Additional Contributions")]
		[Category("API Authorisation")]
		[TestCase(TestName = "No Access Given Invalid APIM Key")]
		public void Given_InvalidAPIMKey_Get_ErrorMessage()
		{
			var serviceHost = ConfigurationManager.AppSettings[$"{Environment()}.Service.Host"];
			
			var requestPayload = new AdditionalContributionsRequest
			{
				AnnualSalary = 50000,
				LumpSumContribution = 10,
				AdditionalContribution = 5,
				DateOfBirth = new DateTime(1970, 06, 01),
				Gender = "m",
				HasVoluntarySalaryCap = false,
				RetirementAge = 65,
				SalaryIncrease = 2.5,
				VoluntarySalaryCapAmount = 0,
				InflationIncrease = 2.5,
				InvestmentReturn = 5,
			};

			var restClient = InitialiseRestClient(serviceHost);
			var request = InitialiseRequest(requestPayload);

			var apiResponse = restClient.Execute<AdditionalContributionsResponse>(request);
			

			Assert.AreEqual(HttpStatusCode.Unauthorized, apiResponse.StatusCode, $"service was accessible despite invalid key");

			
		}
	}
}
