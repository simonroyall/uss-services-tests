﻿using System;
using System.Collections.Generic;
using System.Net;
using NUnit.Framework;
using USS_API_Tests.Modellers.CostModeller;
using System.Configuration;
using FluentAssertions;
using FluentAssertions.Execution;

namespace USS_API_Tests.Tests.CostModellerTests
{
	[TestFixture]
	class CostModellerCalculationTests : CostModellerBaseTest
	{
		private static List<object> _testData = new List<object>();

		[Test]
		[TestCaseSource(nameof(GetTestData))]
		[Category("API Cost Modeller")]
		[Category("API Calculation")]
		public void VerifyValidData(CostModellerTestConfigItem testItem)
		{

			var serviceHost = ConfigurationManager.AppSettings[$"{Environment()}.Service.Host"];


			var requestPayload = new CostModellerRequest
			{
				AnnualSalary = testItem.AnnualSalary,
				AdditionalContributionValue = testItem.AdditionalContributionValue,
				AdditionalContributionType = testItem.AdditionalContributionType,
				FsavcRate = testItem.FsavcRate,
				CrbAvcAmount = testItem.CrbAvcAmount,
				IsSalarySacrifice = testItem.IsSalarySacrifice,
				ShouldUseScottishTaxBrackets = testItem.ShouldUseScottishTaxBrackets,
			};

			var restClient = InitialiseRestClient(serviceHost);
			var request = InitialiseRequest(requestPayload, "costmodeller");

			var apiResponse = restClient.Execute<CostModellerResponse>(request);
			var errors = string.Join(", ", apiResponse.Data?.Errors);

			Assert.AreEqual(HttpStatusCode.OK, apiResponse.StatusCode, $"test failed with errors {errors}");

			var responseData = apiResponse.Data;

			Assert.IsNotNull(responseData);

			Console.WriteLine(testItem.TestName);
			using (new AssertionScope())
			{
				responseData.Data.MemberMonthlySchemeContributions.Should()
					.Be(testItem.ExpectedMemberMonthlySchemeContributions);
				responseData.Data.AnnualAdditionalInvestmentBuilderContributions.Should()
					.Be(testItem.ExpectedMemberAnnualAdditionalInvestmentBuilderContributions);
				responseData.Data.MonthlyAdditionalInvestmentBuilderContributions.Should()
					.Be(testItem.ExpectedMemberMonthlyAdditionalInvestmentBuilderContributions);
				responseData.Data.AnnualNormalInvestmentBuilderContributions.Should()
					.Be(testItem.ExpectedMemberAnnualNormalInvestmentBuilderContributions);
				responseData.Data.MonthlyNormalInvestmentBuilderContributions.Should()
					.Be(testItem.ExpectedMemberMonthlyNormalInvestmentBuilderContributions);
				responseData.Data.EmployerAnnualNormalInvestmentBuilderContributions.Should()
					.Be(testItem.ExpectedEmployerAnnualNormalInvestmentBuilderContributions);
				responseData.Data.AnnualNormalInvestmentBuilderContributions.Should()
					.Be(testItem.ExpectedMemberAnnualNormalInvestmentBuilderContributions);
				responseData.Data.EmployerMonthlyNormalInvestmentBuilderContributions.Should()
					.Be(testItem.ExpectedEmployerMonthlyNormalInvestmentBuilderContributions);
				if (testItem.ShouldUseScottishTaxBrackets)
				{
					responseData.Data.AnnualScottishTaxSaving.Should()
						.Be(testItem.ExpectedAnnualScottishMemberSavingTax);
					responseData.Data.MonthlyScottishTaxSaving.Should()
						.Be(testItem.ExpectedMonthlyScottishMemberSavingTax);
				}
				responseData.Data.AnnualTaxSaving.Should().BeApproximately(testItem.ExpectedAnnualMemberSavingTax,2);
				responseData.Data.MonthlyTaxSaving.Should().BeApproximately(testItem.ExpectedMonthlyMemberSavingTax,2);
				responseData.Data.MonthlyMemberNationalInsuranceSaving.Should()
					.Be(testItem.ExpectedMonthlyMemberSavingNationalInsurance);
				responseData.Data.AnnualMemberNationalInsuranceSaving.Should()
					.Be(testItem.ExpectedAnnualMemberSavingNationalInsurance);
				responseData.Data.MonthlyEmployerContributions.Should().Be(testItem.ExpectedMonthlyEmployerContributions);
				responseData.Data.AnnualEmployerContributions.Should().Be(testItem.ExpectedAnnualEmployerContributions);
				responseData.Data.AnnualNetCost.Should().Be(testItem.ExpectedAnnualNetMemberCost);
				responseData.Data.MonthlyNetCost.Should().Be(testItem.ExpectedMonthlyNetMemberCost);
				Math.Round(responseData.Data.RateOfSavingsRelief,2).Should().Be(testItem.ExpectedRateSavingRelief);
				responseData.Data.AnnualStandardSchemeContributions.Should().BeApproximately(testItem.ExpectedMemberAnnualStandardSchemeContributions,2);
				responseData.Data.MonthlyStandardSchemeContributions.Should().BeApproximately(testItem.ExpectedMemberMonthlyStandardSchemeContributions,2);
				responseData.Data.AnnualAllowanceBreachWarning.Should().Be(testItem.ExpectedAnnualAllowanceBreachWarning);
			}

		}
	}
}
