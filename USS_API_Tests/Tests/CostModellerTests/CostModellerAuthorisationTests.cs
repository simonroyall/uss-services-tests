﻿using NUnit.Framework;
using RestSharp;
using System.Configuration;
using System.Net;
using USS_API_Tests.Modellers.CostModeller;


namespace USS_API_Tests.Tests.CostModellerTests
{
	[TestFixture]
	class CostModellerAuthorisationTests: CostModellerBaseTest
	{
		public override RestRequest InitialiseRequest(CostModellerRequest requestPayload, string resource)
		{
			var request = new RestRequest(resource, Method.POST);
			string badKey = "a1ea0cdb656e43a484549c77b3f7ff09";
			request.AddJsonBody(requestPayload);
			request.AddHeader("Ocp-Apim-Subscription-Key", badKey);
			return request;
		}

		[Test]
		[Category("API Cost Modeller")]
		[Category("API Authorisation")]
		[TestCase(TestName = "No Access to Main Calculation Given Invalid APIM Key")]
		public void Given_Invalid_APIM_Key_Get_ErrorMessage_Main_Calculation()
		{
			var serviceHost = ConfigurationManager.AppSettings[$"{Environment()}.Service.Host"];

			var requestPayload = new CostModellerRequest
			{
				AnnualSalary = 50000,
			};

			var restClient = InitialiseRestClient(serviceHost);
			var request = InitialiseRequest(requestPayload,"costmodeller");

			var apiResponse = restClient.Execute<CostModellerResponse>(request);


			Assert.AreEqual(HttpStatusCode.Unauthorized, apiResponse.StatusCode, "service was accessible despite invalid key");


		}

		[Test]
		[Category("API Cost Modeller")]
		[Category("API Authorisation")]
		[TestCase(TestName = "No Access To Pension Tax Modeller Given Invalid APIM Key")]
		public void Given_Invalid_APIM_Key_Get_ErrorMessage_Pension_Tax()
		{
			var serviceHost = ConfigurationManager.AppSettings[$"{Environment()}.Service.Host"];

			var requestPayload = new CostModellerRequest
			{
				AnnualSalary = 50000,
			};

			var restClient = InitialiseRestClient(serviceHost);
			var request = InitialiseRequest(requestPayload, "costmodeller/pensiontax");

			var apiResponse = restClient.Execute<CostModellerResponse>(request);


			Assert.AreEqual(HttpStatusCode.Unauthorized, apiResponse.StatusCode, "service was accessible despite invalid key");


		}
	}
}
