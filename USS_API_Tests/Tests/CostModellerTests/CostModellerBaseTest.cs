﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using RestSharp;
using USS_API_Tests.Modellers.CostModeller;
using System.Configuration;
using Newtonsoft.Json;
using USS_API_Tests.Serialization;

namespace USS_API_Tests.Tests.CostModellerTests
{
	public abstract class CostModellerBaseTest
	{
		public virtual RestRequest InitialiseRequest(CostModellerRequest requestPayload,string resource)
		{
			var request = new RestRequest(resource, Method.POST);
			request.AddJsonBody(requestPayload);
			request.AddHeader("Ocp-Apim-Subscription-Key", ConfigurationManager.AppSettings[$"{Environment()}.Apim.Subscription.Key"]);
			return request;
		}
		public static RestClient InitialiseRestClient(string serviceHost)
		{
			var restClient = new RestClient(serviceHost);
			restClient.UseSerializer(() => new JsonNetSerializer());
			return restClient;
		}

		public string Environment()
		{
			var where = ConfigurationManager.AppSettings["Environment"];
			var legitimateEnvironments = new List<string>();
			legitimateEnvironments.Add("Test");
			legitimateEnvironments.Add("UAT");
			legitimateEnvironments.Add("Live");

			if (legitimateEnvironments.Contains(where))
				return @where;
			throw new Exception($"'{@where}' is not a legitimate Environment - check the Config file!");
		}

		public static IEnumerable<CostModellerTestConfigItem> GetTestData()
		{

			var testSource = File.ReadAllText(Path.Combine(Directory.GetCurrentDirectory(), $"\\Projects\\UssServices\\USS_API_Tests\\Data\\costModellerMainCalculation.json"));

			var testItems = JsonConvert.DeserializeObject<IEnumerable<CostModellerTestConfigItem>>(testSource);

			if (IsInLive)
			{
				return testItems.Take(5);
			}
			return testItems;
		}

		static bool IsInLive => ConfigurationManager.AppSettings["Environment"] == "Live";

	}
}
